# frozen_string_literal: true

class Potepan::CategoriesController < ApplicationController
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @products = Spree::Product.in_taxon(@taxon).includes(classifications: :product)
    @taxonomies = Spree::Taxonomy.includes(:taxons)
  end
end
