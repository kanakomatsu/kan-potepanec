# frozen_string_literal: true

module Potepan::ProductDecorator
  def related_products(count)
    Spree::Product.in_taxons(taxons)
                  .where.not(id: id)
                  .distinct
                  .order(:id)
                  .limit(count)
  end

  Spree::Product.prepend self
end
