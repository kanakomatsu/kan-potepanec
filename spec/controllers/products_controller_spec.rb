# frozen_string_literal: true

require 'rails_helper'
require 'capybara/rspec'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let!(:product) { create :product }
    before do
      get :show, params: { id: product.id }
    end

    it '200レスポンスを返す' do
      expect(response.status).to eq(200)
    end

    it 'showを描画する' do
      expect(response).to render_template :show
    end

    it '@productが期待される値を持つ' do
      expect(assigns(:product)).to eq(product)
    end
  end
end
