# frozen_string_literal: true

require 'rails_helper'
require 'capybara/rspec'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }
    before do
      get :show, params: { id: taxon.id }
    end

    it '200レスポンスを返す' do
      expect(response.status).to eq(200)
    end

    it 'showを描画する' do
      expect(response).to render_template :show
    end

    it '@taxonが期待される値を持つ' do
      expect(assigns(:taxon)).to eq(taxon)
    end

    it '@taxonomyが期待される値を持つ' do
      expect(assigns(:taxonomies)).to eq([taxonomy])
    end

    it '@productが期待される値を持つ' do
      expect(assigns(:products)).to eq([product])
    end
  end
end
