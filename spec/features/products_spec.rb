# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Products', type: :feature do
  describe 'GET #show' do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:taxon2) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
    let!(:undisplayed_related_product) { create(:product, taxons: [taxon]) }
    let!(:no_related_product) { create(:product, taxons: [taxon2]) }
    before do
      visit potepan_product_path product.id
    end

    it '商品画面を表示する' do
      # productの情報が表示される
      expect(page).to have_content product.name
      expect(page).to have_content product.price
      expect(page).to have_content product.description
      within '.productsContent' do
        # 非関連商品が表示されない
        expect(page).to have_no_content no_related_product.name
        # １〜４つ目の関連商品が表示される
        0.upto(3).each do |index|
          expect(page).to have_content related_products[index].name
          expect(page).to have_content related_products[index].price
        end
        # ５つ目の関連商品が表示されない
        expect(page).to have_no_content no_related_product.name
      end
    end

    it 'Homeボタン押下時、トップ画面に遷移する' do
      within '.lightSection .breadcrumb' do
        click_on 'Home'
      end
      expect(current_path).to eq potepan_path
      expect(page).to have_content '人気カテゴリー'
      expect(page).to have_content '新着商品'
    end

    it 'header Homeボタン押下時、トップ画面に遷移する' do
      within '.navbar .collapse' do
        click_on 'Home'
      end
      expect(current_path).to eq potepan_path
      expect(page).to have_content '人気カテゴリー'
      expect(page).to have_content '新着商品'
    end

    it 'ロゴをクリックした時、トップ画面に遷移する' do
      click_link 'logo'
      expect(current_path).to eq potepan_path
      expect(page).to have_content '人気カテゴリー'
      expect(page).to have_content '新着商品'
    end

    it '関連商品をクリックした時、その商品ページに遷移する' do
      within '.productsContent' do
        click_on related_products.first.name
      end
      expect(current_path).to eq potepan_product_path related_products.first.id
      within '.mainContent' do
        expect(page).to have_content related_products.first.name
        expect(page).to have_content related_products.first.price
        expect(page).to have_content related_products.first.description
      end
    end

    it '一覧ページへ戻るをクリックした時、カテゴリーページに遷移する' do
      click_link '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path taxon.id
      expect(page).to have_content taxon.name
    end
  end
end
