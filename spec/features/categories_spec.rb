# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Categories', type: :feature do
  describe 'GET #show' do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id, name: 'taxon1') }
    let!(:taxon2) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:taxon3) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:product2) { create(:product, taxons: [taxon]) }
    let!(:product3) { create(:product, taxons: [taxon2]) }
    let!(:product4) { create(:product, taxons: [taxon3]) }
    before do
      visit potepan_category_path taxon.id
    end

    it 'カテゴリー情報と商品一覧を表示する' do
      # カテゴリーの情報
      expect(page).to have_content taxon.name
      # カテゴリーの１つ目の商品情報
      expect(page).to have_content product.name
      expect(page).to have_content product.price
      # カテゴリーの２つ目の商品情報
      expect(page).to have_content product2.name
      expect(page).to have_content product2.price
      # 異なるカテゴリーの商品が表示されない
      expect(page).to have_no_content product3.name
      expect(page).to have_no_content product4.name
      # 大カテゴリーの名前
      expect(page).to have_content taxonomy.name
      # 小カテゴリーの名前
      within '.panel-body .navbar-collapse' do
        expect(page).to have_content taxon2.name
        expect(page).to have_content taxon3.name
      end
    end

    it '小カテゴリーの名前をクリックした時、小カテゴリーのページに遷移する' do
      within '.panel-body .navbar-collapse' do
        click_on taxon2.name, match: :first
      end
      expect(current_path).to eq potepan_category_path taxon2.id
      within '.lightSection .breadcrumb' do
        expect(page).to have_content taxon2.name
      end
    end

    it 'カテゴリーの商品をクリックした時、商品画面に遷移する' do
      click_on product.name
      expect(current_path).to eq potepan_product_path product.id
      expect(page).to have_content product.name
      expect(page).to have_content product.price
      expect(page).to have_content product.description
    end

    it 'Homeボタン押下時、トップ画面に遷移する' do
      within '.lightSection .breadcrumb' do
        click_on 'Home'
      end
      expect(current_path).to eq potepan_path
      expect(page).to have_content '人気カテゴリー'
      expect(page).to have_content '新着商品'
    end

    it 'header Homeボタン押下時、トップ画面に遷移する' do
      within '.navbar .collapse' do
        click_on 'Home'
      end
      expect(current_path).to eq potepan_path
      expect(page).to have_content '人気カテゴリー'
      expect(page).to have_content '新着商品'
    end

    it 'ロゴをクリックした時、トップ画面に遷移する' do
      click_link 'logo'
      expect(current_path).to eq potepan_path
      expect(page).to have_content '人気カテゴリー'
      expect(page).to have_content '新着商品'
    end
  end
end
