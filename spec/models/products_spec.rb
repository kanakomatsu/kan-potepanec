# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe 'related_products' do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent_id: taxonomy.root.id) }
    let!(:product) { create(:product, taxons: [taxon]) }

    context '関連商品が4つ存在する場合' do
      let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
      it '関連商品を返す' do
        expect(product.related_products(PRODUCT::RELATED_PRODUCTS_COUNT)).to match_array(related_products)
      end
    end

    context '関連商品が5つ以上存在する場合' do
      let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }
      it '関連商品を返す' do
        expect(product.related_products(PRODUCT::RELATED_PRODUCTS_COUNT)).to match_array(related_products.slice(0, 4))
      end
    end

    context '関連商品が存在しない場合' do
      let!(:no_related_product) { create(:product) }
      it '関連商品を返さない' do
        expect(product.related_products(PRODUCT::RELATED_PRODUCTS_COUNT).count).to eq 0
      end
    end
  end
end
